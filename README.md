# vss

#### Table of Contents

1. [Description](#description)
2. [Setup - The basics of getting started with vss](#setup)
    * [What vss affects](#what-vss-affects)
    * [Setup requirements](#setup-requirements)
    * [Beginning with vss](#beginning-with-vss)
3. [Usage - Configuration options and additional functionality](#usage)
4. [Reference - An under-the-hood peek at what the module is doing and how](#reference)
5. [Limitations - OS compatibility, etc.](#limitations)
6. [Development - Guide for contributing to the module](#development)

## Description

This module adds a provider to manage Volume Shadow Copy Service (VSS) configuration on windows.

## Setup

### Beginning with vss

This module can reserver drive space on a specified drive and create scheduled tasks to take regular snapshots similar to what the vssadmin.exe tool would create if run from the command line.

a basic example of configuring snapshots on the D drive for snapshots every hour starting at 8am would look like this:
```puppet
vss { 'D':
  ensure => 'present',
}
vss::schedule { 'D':
  ensure     => 'present',
  run_schedule   => 'daily',
  start_time => '08:00',
}
```
The example above reserves 10% of the D:\ drive for VSS storage and sets up scheduled tasks to take a new snapshot every 60 minutes.

## Usage

A more complete example of creating VSS schedules including managing allocated space and assigned drives would look like this:

```puppet
vss { 'D':
  ensure         => 'present',
  storage_volume => 'D',
  storage_space  => 5,
}
vss::schedule { 'C':
  ensure       => present,
  run_schedule => 'daily',
  start_time   => '00:00',
  start_date   => '2018-01-01',
  minutes_interval => 45,
  minutes_duration => 5,
  
}
```

the vss::schedule resource is a direct implementation of the builtin [scheduled_task trigger](https://puppet.com/docs/puppet/5.5/types/scheduled_task.html#scheduled_task-attribute-trigger) attribute so anything that can be set for a trigger can also be set for a schedule here. 

## Reference

* [vss provider](#vss_provider)
* [vss::schedule](#vss::schedule)

### vss provider

#### Properties and Parameters
creates vss schedules and storage.

#### `ensure`
accepts either present or absent. this decides if space should be allocated for VSS or not. 

#### `name`
the name of the drive that you wish to backup. This is also the namevar

#### `drive_id`
this is the UUID of the drive being backed up. It cannot be changed.

#### `storage_volume`
the drive where the VSS storage will be alocated. By default this is the same drive as the name parameter but it can be set to somewhere else:
```puppet
vss { 'D':
  ensure         => 'present',
  storage_volume => 'E',
}
```
#### `storage_id`
The UUID of the storage_volume. This cannot be changed.

#### `storage_space`
The amount of a drive to reserve for VSS storage as a percent of the total drive. 
```puppet
vss { 'D':
  ensure         => 'present',
  storage_volume => 'D',
  storage_space  => 5,
}
```
would allocate 5% of the D:\ drive for storage.

### vss::schedule
The vss::schedule defined type acts as a wrapper for the built-in [scheduled_task](https://puppet.com/docs/puppet/5.5/types/scheduled_task.html) resource. Any setting that would be used with the [trigger attribute](https://puppet.com/docs/puppet/5.5/types/scheduled_task.html#scheduled_task-attribute-trigger) can be used with vss::schedule. The only limitation to this is that only one trigger can be defined at a time per drive. 

## Limitations

at this time only one schedule per drive can be defined. 

## Development

Any and all changes are welcome! please open a pull request and include unit tests for relevant changes. 

