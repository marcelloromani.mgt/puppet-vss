#  Creates scheduled tasks for when VSS snapshots wlil occur
#
# @summary This schedules when VSS snapshots wlil be taken
#
# @example
#   vss::schedule { 'C':
#     ensure       => 'present',
#     run_schedule => 'daily',
#     start_time   => 08:00,
#   }
define vss::schedule(
  Enum['daily', 'weekly', 'monthly', 'once'] $run_schedule,
  Pattern[/\d{2}:\d{2}/] $start_time,
  Enum['absent', 'present'] $ensure = 'present',
  Pattern[/\d{4}-\d{2}-\d{2}/] $start_date = strftime('%Y-%m-%d'),
  Integer $minutes_interval = 1,
  Integer $minutes_duration = 5,
  Integer $every = 1,
  Optional[Array[Integer[1,31]]] $on = undef,
  Optional[Enum['first', 'second', 'third', 'fourth', 'fifth']] $which_occurrence = undef,
  Optional[Array[Enum['mon', 'tues', 'wed', 'thurs', 'fri', 'sat', 'sun']]] $day_of_week =undef,
  Array[Integer[1,12]] $months = [1,2,3,4,5,6,7,8,9,10,11,12],
) {
  $base_trigger = {
      schedule         => $run_schedule,
      start_time       => $start_time,
      start_date       => $start_date,
      minutes_interval => $minutes_interval,
      minutes_duration => $minutes_duration,
  }
  case $run_schedule {
    'daily':   { $final_trigger = $base_trigger + { every => $every } }
    'weekly':  { $final_trigger = $base_trigger + {
                  every       => $every,
                  day_of_week => $day_of_week,
                  }
                }
    'monthly': { $final_trigger = if $on {
                                      $base_trigger + {
                                        months => $months,
                                        on     => $on,
                                      }
                                    }
                                    else {
                                      $base_trigger + {
                                        day_of_week      => $day_of_week,
                                        which_occurrence => $which_occurrence,
                                      }
                                    }
              }
    'once':   { $final_trigger = $base_trigger }
    default:  {}
  }

  scheduled_task { "ShadowCopy{${title}}":
    ensure    => $ensure,
    command   => 'C:\\Windows\\system32\\vssadmin.exe',
    arguments => "Create Shadow /AutoRetry=15 /For=${title}:",
    trigger   => $final_trigger
  }
}
