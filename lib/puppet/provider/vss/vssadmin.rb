require 'puppet/util/windows/taskscheduler'

Puppet::Type.type(:vss).provide(:vssadmin) do
  desc 'This implements vssadmin commands to manage VSS settings on a node.'

  confine operatingsystem: :windows
  defaultfor operatingsystem: :windows

  commands vssadmin_exe: 'vssadmin.exe'

  mk_resource_methods

  # set up the @property_flush hash to be used later on to sync all
  #   settings that are not configured properly.
  def initialize(value = {})
    super(value)
    @property_flush = {}
  end

  # This collects data on storage in use and allocated for VSS use.
  #   As well as the current schedule that new snapshots are taken.
  #
  # @return [Array[Hash]] An array of hashes with the name of the drive,
  #   it's storage drive and the storage space allocated as a percent and
  #   the schedule snapshots are taken.
  def self.collect_storage_info(drive = :all)
    begin
      storage = vssadmin_exe(['list', 'shadowstorage']).split("\n\n")[1]
    rescue Puppet::ExecutionFailure => e
      Puppet.debug("# Execution Failed -> #{e.inspect}")
      return nil
    end
    info = []
    hash = {}
    storage.split("\n").each do |line|
      case line.strip
      when %r{^For}
        if hash.key?(:name)
          hash.clear # if the key is there this is a new resource. clear the old values
        end
        hash[:provider] = :vssadmin
        hash[:name] = line[16]
        hash[:drive_id] = line.split('{')[1][0..-3]
      when %r{^Shadow .*:}
        hash[:storage_volume] = line.split(':')[1][2]
        hash[:storage_id] = line.split('{')[1][0..-3]
        hash[:ensure] = hash.key?(:storage_volume) ? 'present' : 'absent'
      when %r{^Maximum}
        hash[:storage_space] = "#{line.split('(')[1][0]}%"
        if drive == (:all || hash[:name])
          info << hash # this is the last line of data for a resource so push it into the array
        end
      end
    end
    info
  end

  # Used to collect all instances of vss for each drive on the system. Currently the schedule
  #   parameter is not returned unless it is already managed from puppet
  #
  # @return [Array] an array of all vss resources
  def self.instances
    collect_storage_info.map do |info|
      new(info)
    end
  end

  # Used to collect all instances of vss for each drive on the system. Currently the schedule
  #   parameter is not returned unless it is already managed from puppet
  #
  # @return [Array] an array of all vss resources
  def self.prefetch(resources)
    instances.each do |prov|
      if resource == resources[prov.name]
        resource.provider = prov
      end
    end
  end

  # Wrapper to set a value for flush to use
  def create
    @property_flush[:ensure] = 'present'
  end

  # Tests if the resource already exists or not
  def exists?
    @property_hash[:ensure] == 'present'
  end

  # Wrapper to set a value for flush to use
  def destroy
    @property_flush[:ensure] = 'absent'
  end

  # method to set storage settings properly
  def set_storage
    size = if @property_flush[:ensure] == 'absent'
             '320MB'
           else
             resource[:storage_space]
           end
    vssadmin_exe('resize', 'shadowstorage', "/For=#{resource[:drive]}:", "/ON=#{resource[:storage_volume]}:", "/MaxSize=#{size}")
  end

  # the method to handle modifing the system.
  #   a call is made to set_storage to ensure storage is configured properly.
  #   finally the @property_hash is updated with the new values
  def flush
    set_storage

    @property_hash = self.class.collect_storage_info(resource[:drive])
  end
end
